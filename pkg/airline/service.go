/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package airline

import (
	"errors"

	"gitlab.com/openairliner/core/pkg/aircraft"
)

// ErrInvalidArgument is returned when one or more arguments are invalid.
var ErrInvalidArgument = errors.New("invalid argument")

//ErrNotFound is returned when no airline could be found.
var ErrNotFound = errors.New("airline not found")

// ErrNotImplemented is returned when a function is not implemented.
var ErrNotImplemented = errors.New("this function is not implemented")

// Service is the interface that provides Airline methods.
type Service interface {
	// Airlines returns a list of all airlines that exist.
	Airlines() []Airline

	// Find will query the repository of airlines and try to find a matching ID or Name
	Find(q string) ([]Airline, error)

	AddAircraftToAirline(aid ID, rid aircraft.ID) error
	GetAircrafts(aid ID) ([]aircraft.ID, error)
}

// NewService creates a airline service with necessary dependencies.
func NewService(airlines Repository) Service {
	return &service{
		airlines:  airlines,
		aircrafts: make(map[ID][]aircraft.ID),
	}
}

type service struct {
	airlines  Repository
	aircrafts map[ID][]aircraft.ID
}

func (s *service) Airlines() []Airline {
	var result []Airline

	for _, a := range s.airlines.FindAll() {
		result = append(result, *a)
	}

	return result
}

func (s *service) Find(q string) ([]Airline, error) {
	var result []Airline

	for _, a := range s.airlines.FindAll() {
		if a.Name == q {
			result = append(result, *a)
		}
	}

	a, _ := s.airlines.Find(ID(q))
	if a != nil {
		result = append(result, *a)
	}

	if len(result) == 0 {
		return nil, ErrNotFound
	}

	return result, nil
}

// TODO we need to check if the airline and aircraft exist
func (s *service) AddAircraftToAirline(aid ID, rid aircraft.ID) error {
	_, ok := s.aircrafts[aid]
	if !ok {
		var aircraft []aircraft.ID
		s.aircrafts[aid] = aircraft
	}

	s.aircrafts[aid] = append(s.aircrafts[aid], rid)

	return nil
}

func (s *service) GetAircrafts(aid ID) ([]aircraft.ID, error) {
	return s.aircrafts[aid], nil
}
