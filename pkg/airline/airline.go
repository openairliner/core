/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package airline

import (
	"errors"
)

// ID uniquely identifies a particular airline.
type ID string

// Airline is... an airline.
type Airline struct {
	ID   ID     `json:"ID"`
	Name string `json:"Name"`
}

// New creates a new airline.
func New(id ID, name string) *Airline {
	return &Airline{
		ID:   id,
		Name: name,
	}
}

// Repository provides access an airline store.
type Repository interface {
	Store(airline *Airline) error
	Update(a *Airline) error
	Delete(id ID) (*Airline, error)
	Find(id ID) (*Airline, error)
	FindAll() []*Airline
}

// ErrUnknown is used when an airline could not be found.
var ErrUnknown = errors.New("unknown airline")
