/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package airline

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/openairliner/core/pkg/aircraft"

	"github.com/gorilla/mux"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

// MakeHandler returns a handler for the airlines service.
func MakeHandler(s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(encodeError),
	}
	listAirlinesHandler := kithttp.NewServer(
		makeListAirlinesEndpoint(s),
		decodeListAirlinesRequest,
		encodeResponse,
		opts...,
	)
	findAirlineHandler := kithttp.NewServer(
		makeFindAirlineEndpoint(s),
		decodeFindAirlineRequest,
		encodeResponse,
		opts...,
	)
	listAddAircraftToAirlineHandler := kithttp.NewServer(
		makeAddAircraftToAirlineEndpoint(s),
		decodeAddAircraftToAirlineRequest,
		encodeResponse,
		opts...,
	)
	listAircraftOfAirlineHandler := kithttp.NewServer(
		makeAircraftOfAirlineEndpoint(s),
		decodeListAircraftOfAirlineRequest,
		encodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle("/airlines/", listAirlinesHandler).Methods("GET")
	r.Handle("/airlines/{q}", findAirlineHandler).Methods("GET")
	r.Handle("/airlines/{aid}/add_aircraft", listAddAircraftToAirlineHandler).Methods("POST")
	r.Handle("/airlines/{aid}/aircrafts/", listAircraftOfAirlineHandler).Methods("GET")

	return r
}

var errBadRoute = errors.New("bad route")

func decodeListAirlinesRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return listAirlinesRequest{}, nil
}

func decodeFindAirlineRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)

	q, ok := vars["q"]
	if !ok {
		return nil, errBadRoute
	}

	return findAirlineRequest{
		q: q,
	}, nil
}

func decodeAddAircraftToAirlineRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)

	aid, ok := vars["aid"]
	if !ok {
		return nil, errBadRoute
	}

	var rid string
	if err := json.NewDecoder(r.Body).Decode(&rid); err != nil {
		return nil, err
	}

	return addAircraftToAirlineRequest{
		Aid: ID(aid),
		Rid: aircraft.ID(rid),
	}, nil
}

func decodeListAircraftOfAirlineRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)

	aid, ok := vars["aid"]
	if !ok {
		return nil, errBadRoute
	}

	return listAircraftOfAirlineRequest{
		Aid: ID(aid),
	}, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	return json.NewEncoder(w).Encode(response)
}

type errorer interface {
	error() error
}

// encode errors from business-logic
func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	switch err {
	case ErrUnknown:
		w.WriteHeader(http.StatusNotFound)
	case ErrInvalidArgument:
		w.WriteHeader(http.StatusBadRequest)
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
