/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package airport

import (
	"context"
	"errors"
	"strings"
)

// ErrInvalidArgument is returned when one or more arguments are invalid.
var ErrInvalidArgument = errors.New("invalid argument")
var ErrAlreadyExists = errors.New("airport already exists")
var ErrNotFound = errors.New("airport not found")

// ErrNotImplemented is returned when a function is not implemented.
var ErrNotImplemented = errors.New("this function is not implemented")

// Service is the interface that provides Airline methods.
type Service interface {
	// Airports returns a list of all airports that exist.
	Airports() []Airport

	// GetAirportByICAO this will get/find an airport by its ICAO code
	GetAirportByICAO(ctx context.Context, icao string) (*Airport, error)
}

type service struct {
	airports Repository
}

// Airports returns a list of all airports that exist.
func (s *service) Airports() []Airport {
	var result []Airport

	for _, a := range s.airports.FindAll() {
		result = append(result, *a)
	}

	return result
}

// GetAirportByICAO this will get/find an airport by its ICAO code
func (s *service) GetAirportByICAO(_ context.Context, icao string) (*Airport, error) {
	// look up airport by it's ICAO code
	a, err := s.airports.Find(ID(strings.ToUpper(icao)))
	if err != nil {
		return nil, ErrNotFound
	}

	return a, nil
}

// NewService creates a airport service with necessary dependencies.
func NewService(r Repository) Service {
	return &service{
		airports: r,
	}
}
