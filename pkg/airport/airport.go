/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package airport

// ID uniquely identifies a particular airport.
type ID string

// Airport represents a single airport.
// ID is globally unique.
type Airport struct {
	ID        ID     `json:"id"`
	Name      string `json:"name"`
	City      string `json:"city"`
	Country   string `json:"country"`
	IATA      string `json:"iata"`
	ICAO      string `json:"icao"`
	Latitude  string `json:"lat"`
	Longitude string `json:"lon"`
	Altitude  int32  `json:"alt"`
	Tz        string `json:"tz"`
	Type      string `json:"type,omitempty"`
}

// New creates a new airport.
func New(id ID, icao string) *Airport {
	return &Airport{
		ID:   id,
		ICAO: icao,
	}
}

// Repository provides access an airport store.
type Repository interface {
	Store(a *Airport) error
	Update(a *Airport) error
	Delete(id ID) (*Airport, error)
	Find(id ID) (*Airport, error)
	FindAll() []*Airport
}
