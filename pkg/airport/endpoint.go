/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package airport

import (
	"context"

	"github.com/go-kit/kit/endpoint"
)

type listAllAirportsRequest struct {
}

type getAirportByICAORequest struct {
	ICAO string `json:"icao"`
}

type listAllAirportsResponse struct {
	Airports []Airport `json:"airports,omitempty"`
	Err      error     `json:"error,omitempty"`
}

type getAirportByICAOResponse struct {
	Airport Airport `json:"airport,omitempty"`
	Err     error   `json:"error,omitempty"`
}

func (r getAirportByICAOResponse) error() error { return r.Err }

func makeListAllAirportsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(listAllAirportsRequest)

		return listAllAirportsResponse{Airports: s.Airports(), Err: nil}, nil
	}
}

func makeGetAirportByICAOEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getAirportByICAORequest)

		a, err := s.GetAirportByICAO(ctx, req.ICAO)
		switch err {
		case ErrNotFound:
			return getAirportByICAOResponse{Err: err}, err
		case ErrNotImplemented:
			return getAirportByICAOResponse{Err: err}, err
		}

		return getAirportByICAOResponse{Airport: *a, Err: nil}, nil
	}
}
