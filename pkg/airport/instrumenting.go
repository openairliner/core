/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package airport

import (
	"context"
	"time"

	"github.com/go-kit/kit/metrics"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(counter metrics.Counter, latency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   counter,
		requestLatency: latency,
		Service:        s,
	}
}

func (s *instrumentingService) Airports() []Airport {
	defer func(begin time.Time) {
		s.requestCount.With("method", "list_airports").Add(1)
		s.requestLatency.With("method", "list_airports").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Airports()
}

func (s *instrumentingService) GetAirportByICAO(ctx context.Context, icao string) (*Airport, error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "get_airport_by_icao").Add(1)
		s.requestLatency.With("method", "get_airport_by_icao").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetAirportByICAO(ctx, icao)
}
