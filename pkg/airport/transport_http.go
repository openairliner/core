/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package airport

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

// MakeHandler returns a handler for the airports service.
func MakeHandler(s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(encodeError),
	}

	listAllAirportsHandler := kithttp.NewServer(
		makeListAllAirportsEndpoint(s),
		decodeListAllAirportsRequest,
		encodeResponse,
		opts...,
	)
	getAirportByICAOHandler := kithttp.NewServer(
		makeGetAirportByICAOEndpoint(s),
		decodeGetAirportByICAOHandlerRequest,
		encodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle("/airports/", listAllAirportsHandler).Methods("GET")
	r.Handle("/airports/{icao}", getAirportByICAOHandler).Methods("GET")

	return r
}

var errBadRoute = errors.New("bad route")

func decodeListAllAirportsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return listAllAirportsRequest{}, nil
}

func decodeGetAirportByICAOHandlerRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)

	icao, ok := vars["icao"]
	if !ok {
		return nil, errBadRoute
	}

	return getAirportByICAORequest{
		ICAO: icao,
	}, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	return json.NewEncoder(w).Encode(response)
}

type errorer interface {
	error() error
}

// encode errors from business-logic
func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	switch err {
	case ErrNotFound:
		w.WriteHeader(http.StatusNotFound)
	case ErrInvalidArgument:
		w.WriteHeader(http.StatusBadRequest)
	case ErrNotImplemented:
		w.WriteHeader(http.StatusInternalServerError)
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
