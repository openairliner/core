/*
    OpenAirportr Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package repository

import (
	"os"
	"strings"
	"sync"

	"github.com/gocarina/gocsv"

	"gitlab.com/openairliner/core/pkg/airport"
)

type airportRepository struct {
	mtx      sync.RWMutex
	airports map[airport.ID]*airport.Airport
}

type importedAirport struct {
	ID        string `csv:"ID" json:"id"`
	Name      string `json:"name"`
	City      string `json:"city"`
	Country   string `json:"country"`
	IATA      string `json:"iata"`
	ICAO      string `json:"icao"`
	Latitude  string `json:"lat"`
	Longitude string `json:"lon"`
	Altitude  int32  `json:"alt"`
	NotUsed1  string `csv:"-"`
	NotUsed2  string `csv:"-"`
	Tz        string `json:"tz"`
	Type      string `json:"type,omitempty"`
	NotUsed3  string `csv:"-"`
}

// NewAirportRepository returns a new instance of a in-memory airport repository.
func NewAirportRepository() airport.Repository {
	return &airportRepository{
		airports: make(map[airport.ID]*airport.Airport),
	}
}

func (r *airportRepository) Store(a *airport.Airport) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()

	r.airports[a.ID] = a

	return nil
}

func (r *airportRepository) Update(a *airport.Airport) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()

	if _, ok := r.airports[a.ID]; ok {
		return airport.ErrNotFound
	}

	r.airports[a.ID] = a

	return nil
}

func (r *airportRepository) Delete(id airport.ID) (*airport.Airport, error) {
	r.mtx.Lock()
	defer r.mtx.Unlock()

	if val, ok := r.airports[id]; ok {
		delete(r.airports, id)
		return val, nil
	}

	return nil, airport.ErrNotFound
}

func (r *airportRepository) Find(id airport.ID) (*airport.Airport, error) {
	r.mtx.RLock()
	defer r.mtx.RUnlock()

	if len(r.airports) == 0 {
		r.loadAirportsFromCSV()
	}

	if val, ok := r.airports[id]; ok {
		return val, nil
	}

	return nil, airport.ErrNotFound
}

func (r *airportRepository) FindAll() []*airport.Airport {
	r.mtx.RLock()
	defer r.mtx.RUnlock()

	if len(r.airports) == 0 {
		r.loadAirportsFromCSV()
	}

	a := make([]*airport.Airport, 0, len(r.airports))
	for _, val := range r.airports {
		a = append(a, val)
	}

	return a
}

func (r *airportRepository) loadAirportsFromCSV() {
	airportsFile, err := os.OpenFile("airports.dat", os.O_RDONLY, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer airportsFile.Close()

	aps := []*importedAirport{}

	if err := gocsv.UnmarshalFile(airportsFile, &aps); err != nil {
		panic(err)
	}
	for _, apss := range aps {
		r.airports[airport.ID(strings.ToUpper(apss.ICAO))] = &airport.Airport{
			ID:        airport.ID(strings.ToUpper(apss.ICAO)),
			IATA:      strings.ToUpper(apss.IATA),
			ICAO:      strings.ToUpper(apss.ICAO),
			Name:      apss.Name,
			City:      apss.City,
			Country:   apss.Country,
			Latitude:  apss.Latitude,
			Longitude: apss.Longitude,
			Altitude:  apss.Altitude,
			Tz:        apss.Tz,
			Type:      apss.Type,
		}
	}

}
