/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package repository

import (
	"sync"

	"gitlab.com/openairliner/core/pkg/aircraft"
)

type aircraftRepository struct {
	mtx       sync.RWMutex
	aircrafts map[aircraft.ID]*aircraft.Aircraft
}

// NewAircraftRepository returns a new instance of a in-memory aircraft repository.
func NewAircraftRepository() aircraft.Repository {
	return &aircraftRepository{
		aircrafts: make(map[aircraft.ID]*aircraft.Aircraft),
	}
}

func (r *aircraftRepository) Store(a *aircraft.Aircraft) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()

	r.aircrafts[a.RID] = a

	return nil
}

func (r *aircraftRepository) Update(a *aircraft.Aircraft) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()

	if _, ok := r.aircrafts[a.RID]; ok {
		return aircraft.ErrNotFound
	}

	r.aircrafts[a.RID] = a

	return nil
}

func (r *aircraftRepository) Delete(id aircraft.ID) (*aircraft.Aircraft, error) {
	r.mtx.Lock()
	defer r.mtx.Unlock()

	if val, ok := r.aircrafts[id]; ok {
		delete(r.aircrafts, id)
		return val, nil
	}

	return nil, aircraft.ErrNotFound
}

func (r *aircraftRepository) Find(id aircraft.ID) (*aircraft.Aircraft, error) {
	r.mtx.RLock()
	defer r.mtx.RUnlock()

	if val, ok := r.aircrafts[id]; ok {
		return val, nil
	}

	return nil, aircraft.ErrNotFound
}

func (r *aircraftRepository) FindAll() []*aircraft.Aircraft {
	r.mtx.RLock()
	defer r.mtx.RUnlock()

	a := make([]*aircraft.Aircraft, 0, len(r.aircrafts))
	for _, val := range r.aircrafts {
		a = append(a, val)
	}

	return a
}
