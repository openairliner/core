/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package aircraft

import (
	"errors"
)

// ID uniquely identifies a particular aircraft.
type ID string

// Aircraft represents a single Aircraft.
// ID should be globally unique.
type Aircraft struct {
	RID ID `json:"rid"`
}

// New creates a new aircraft.
func New(rid ID) *Aircraft {
	return &Aircraft{
		RID: rid,
	}
}

// Repository provides access an airline store.
type Repository interface {
	Store(a *Aircraft) error
	Update(a *Aircraft) error
	Delete(id ID) (*Aircraft, error)
	Find(id ID) (*Aircraft, error)
	FindAll() []*Aircraft
}

// Errors related to Aircrafts
var (
	ErrNotFound      = errors.New("unknown aircraft")
	ErrAlreadyExists = errors.New("aircraft already exists")
)
